### Prueba técnica - Pagomio

#### Problema
Pagomío requiere crear una nueva funcionalidad para su plataforma transaccional. Esta está enfocada en permitir que sus administradores tengan la posibilidad de asignar los métodos de pagos disponibles (Visa, Mastercard, American Express) a sus marcas existentes (Marca1,Marca2), además de poder definir la comisión que tendrá cada marca para cada método de pago asignado.
-----------------------
#### Requisitos
- Usar Symfony2 ultima version.
- Crear Bundle PaymentBundle
- Crear entidades necesarias para solucionar el problema.
- Crear CRUD para la entidad marca (Listar,Agregar, Editar, Eliminar).
- Usar Bootstrap para las interfaces gráficas.
-----------------------
#### Implementación
- Importar la base de datos incluida en la carpeta DATABASE
- Por consola, ir a la ruta en la que se encuentra el proyecto. Entrar a la carpeta SITE y ejecutar los comandos `composer update` y luego `php app/console server:run`
- Una vez iniciado el proyecto con el ultimo comando, ingresar a la url http://localhost:8000
-----------------------
Las credenciales de administrador son:

- Url: http://localhost:8000/login
- Usuario: administrator
- Contraseña: Pagomio.2017

#### Resumen del desarrollo
- **Lenguaje:** PHP 7
- **Framework:** Symfony 2.8.23
- **Bases de datos:** MySQL 5.5.42