<?php

namespace PaymentBundle\Controller;

use PaymentBundle\Entity\Brand;
use PaymentBundle\Entity\Payment_Method;
use PaymentBundle\Entity\Brand_Payment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Brand_payment controller.
 *
 * @Route("brand_payment")
 */
class Brand_PaymentController extends Controller
{
    /**
     * Lists all brand_Payment entities of the given brand.
     *
     * @Route("/{brand}", name="brand_payment_index")
     * @Method("GET")
     */
    public function indexAction(Brand $brand)
    {
        return $this->render('brand_payment/index.html.twig', array(
            'brand_Payments' => $brand->getBrandPayment(),
            'brand' => $brand
        ));
    }

    /**
     * Creates a new brand_Payment entity.
     *
     * @Route("/new/{brand}", name="brand_payment_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, Brand $brand)
    {
        $brand_Payment = new Brand_payment();
        $brand_Payment->setBrand($brand);
        $brand_Payment->setIdBrand($brand->getId());
        
        $form = $this->createForm('PaymentBundle\Form\Brand_PaymentType', $brand_Payment);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $payment_methods = $em->getRepository('PaymentBundle:Payment_Method')->findAll();

        if ($form->isSubmitted() && $form->isValid()) {
            // Validate duplicated items
            $exists = $em->createQuery(
                'SELECT COUNT(bp.idBrand)
                FROM PaymentBundle\Entity\Brand_payment bp
                WHERE bp.idBrand = :brand AND bp.idPayment = :payment'
            )->setParameter('brand', $brand->getId())
            ->setParameter('payment', $brand_Payment->getIdPayment());

            if ($exists->getSingleScalarResult() == 0) {
                $brand_Payment->setPaymentMethod($em->getRepository('PaymentBundle:Payment_Method')->find($brand_Payment->getIdPayment()));
                $em->persist($brand_Payment);
                $em->flush();

                return $this->redirectToRoute('brand_payment_index', array('brand' => $brand->getId()));
            }
            $session = $this->get('session');
            $session->getFlashBag()->add('warning', 'No se pueden duplicar los métodos de pago');

        }

        return $this->render('brand_payment/new.html.twig', array(
            'brand_Payment' => $brand_Payment,
            'payment_methods' => $payment_methods,
            'form' => $form->createView(),
            'brand' => $brand
        ));
    }

    /**
     * Displays a form to edit an existing brand_Payment entity.
     *
     * @Route("/{brand}/{payment_method}/edit", name="brand_payment_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Brand $brand, Payment_Method $payment_method)
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQuery(
                'SELECT bp
                FROM PaymentBundle\Entity\Brand_payment bp
                WHERE bp.idBrand = :brand AND bp.idPayment = :payment'
            )->setParameter('brand', $brand->getId())
            ->setParameter('payment', $payment_method->getId());

        //ArrayCollection
        $brand_Payment = $qb->getOneOrNullResult();

        if (is_null($brand_Payment))
        {
            return $this->redirectToRoute('homepage');
        }
        $deleteForm = $this->createDeleteForm($brand, $payment_method);
        $editForm = $this->createForm('PaymentBundle\Form\EditBrand_PaymentType', $brand_Payment);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $brand_Payment->setUpdatedAt(new \DateTime());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('brand_payment_index', array('brand' => $brand->getId()));
        }

        return $this->render('brand_payment/edit.html.twig', array(
            'brand_Payment' => $brand_Payment,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'brand' => $brand,
            'payment_method' => $payment_method
        ));
    }

    /**
     * Deletes a brand_Payment entity.
     *
     * @Route("/{brand}/{payment_method}", name="brand_payment_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Brand $brand, Payment_Method $payment_method)
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQuery(
                'SELECT bp
                FROM PaymentBundle\Entity\Brand_payment bp
                WHERE bp.idBrand = :brand AND bp.idPayment = :payment'
            )->setParameter('brand', $brand->getId())
            ->setParameter('payment', $payment_method->getId());

        //ArrayCollection
        $brand_Payment = $qb->getOneOrNullResult();

        $form = $this->createDeleteForm($brand, $payment_method);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($brand_Payment);
            $em->flush();
        }

        return $this->redirectToRoute('brand_payment_index', array('brand' => $brand->getId()));
    }

    /**
     * Creates a form to delete a brand_Payment entity.
     *
     * @param Brand_Payment $brand_Payment The brand_Payment entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Brand $brand, Payment_Method $payment_method)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('brand_payment_delete', array('brand' => $brand->getId(), 'payment_method' => $payment_method->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
