<?php

namespace PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Payment Method model
 *
 * @ORM\Table(name="payment_methods")
 * @ORM\Entity(repositoryClass="PaymentBundle\Repository\Payment_MethodRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Payment_Method
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="Brand_Payment", mappedBy="Payment_Method", cascade={"persist"})
     */
    private $brandPayment;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->brandPayment = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Payment_Method
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Payment_Method
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Payment_Method
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add brandPayment
     *
     * @param \PaymentBundle\Entity\Brand_Payment $brandPayment
     * @return Payment_Method
     */
    public function addBrandPayment(\PaymentBundle\Entity\Brand_Payment $brandPayment)
    {
        $this->brandPayment[] = $brandPayment;

        return $this;
    }

    /**
     * Remove brandPayment
     *
     * @param \PaymentBundle\Entity\Brand_Payment $brandPayment
     */
    public function removeBrandPayment(\PaymentBundle\Entity\Brand_Payment $brandPayment)
    {
        $this->brandPayment->removeElement($brandPayment);
    }

    /**
     * Get brandPayment
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBrandPayment()
    {
        return $this->brandPayment;
    }
}
