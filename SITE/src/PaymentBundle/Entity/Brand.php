<?php

namespace PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Brand model
 * 
 * @ORM\Table(name="brands")
 * @ORM\Entity(repositoryClass="PaymentBundle\Repository\BrandRepository")
 * @ORM\HasLifecycleCallbacks()
 */
 class Brand
 {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_name", type="string", length=255)
     */

    /**
     * @var string
     *
     * @ORM\Column(name="contact_email", type="string", length=255)
     */
    private $contact_email;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_phone", type="string", length=13)
     */
    private $contact_phone;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_address", type="string", length=100)
     */
    private $contact_address;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
 
    /**
     * @ORM\OneToMany(targetEntity="Brand_Payment", mappedBy="brand", cascade={"persist"})
     */
    private $brandPayment;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->brandPayment = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Brand
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set contact_email
     *
     * @param string $contactEmail
     * @return Brand
     */
    public function setContactEmail($contactEmail)
    {
        $this->contact_email = $contactEmail;

        return $this;
    }

    /**
     * Get contact_email
     *
     * @return string 
     */
    public function getContactEmail()
    {
        return $this->contact_email;
    }

    /**
     * Set contact_phone
     *
     * @param string $contactPhone
     * @return Brand
     */
    public function setContactPhone($contactPhone)
    {
        $this->contact_phone = $contactPhone;

        return $this;
    }

    /**
     * Get contact_phone
     *
     * @return string 
     */
    public function getContactPhone()
    {
        return $this->contact_phone;
    }

    /**
     * Set contact_address
     *
     * @param string $contactAddress
     * @return Brand
     */
    public function setContactAddress($contactAddress)
    {
        $this->contact_address = $contactAddress;

        return $this;
    }

    /**
     * Get contact_address
     *
     * @return string 
     */
    public function getContactAddress()
    {
        return $this->contact_address;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Brand
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Brand
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add brandPayment
     *
     * @param \PaymentBundle\Entity\Brand_Payment $brandPayment
     * @return Brand
     */
    public function addBrandPayment(\PaymentBundle\Entity\Brand_Payment $brandPayment)
    {
        $this->brandPayment[] = $brandPayment;

        return $this;
    }

    /**
     * Remove brandPayment
     *
     * @param \PaymentBundle\Entity\Brand_Payment $brandPayment
     */
    public function removeBrandPayment(\PaymentBundle\Entity\Brand_Payment $brandPayment)
    {
        $this->brandPayment->removeElement($brandPayment);
    }

    /**
     * Get brandPayment
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBrandPayment()
    {
        return $this->brandPayment;
    }
}
